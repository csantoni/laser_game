﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(LineRenderer))]

public class test_laser : MonoBehaviour {
	
	public float laserWidth = 1.0f;
	public float noise = 1.0f;
	public float maxLength = 50.0f;
	public Color color = Color.red;

	public int maxDepth = 3;
	public float resolution = 0.2f;
	public float magnitude = 10f;
	public float scale = 1f;

	public ParticleSystem endEffect;

	LineRenderer lineRenderer;
	float length;
	Transform myTransform;

	Vector3[] intersections;

	void Start () {
		lineRenderer = GetComponent<LineRenderer>();

		lineRenderer.SetWidth(laserWidth, laserWidth);
		myTransform = transform;
	}
	
	void Update () {
		RenderLaser();
	}
	
	void RenderLaser(){
		Vector3[] laser = ComputePath(myTransform.position, myTransform.forward);
		DrawLaser(laser);
	}

	void DrawLaser(Vector3[] path){
		lineRenderer.SetWidth(laserWidth, laserWidth);
		lineRenderer.SetColors(color,color);

		//subdivide path according to resolution
		ArrayList subdPath = new ArrayList();
		subdPath.Add(path[0]);
		for (int i = 0; i < path.Length - 1; i++) {
			float dist = (path[i + 1] - path[i]).magnitude;
			int step = (int)Mathf.Floor(dist / resolution);
			Vector3 p = new Vector3();
			for (int k = 0; k < step; k++){
				p = path[i] + k * resolution * Vector3.Normalize(path[i+1] - path[i]);
				p.x += Random.Range(-noise, noise);
				p.z += Random.Range(-noise, noise);
				subdPath.Add(p);
			}
			subdPath.Add(path[i+1]);
		}

		//Perturb line
		ArrayList final = new ArrayList();
		for (int i = 0; i < subdPath.Count; i++) {
//			float xCoord = ((Vector3)subdPath[i]).x / magnitude * scale + Time.realtimeSinceStartup;
//			float zCoord = ((Vector3)subdPath[i]).z / magnitude * scale + Time.realtimeSinceStartup;
//			float sample = Mathf.PerlinNoise(xCoord, zCoord);
//			final.Add(new Vector3( ((Vector3)subdPath[i]).x + sample, ((Vector3)subdPath[i]).y, ((Vector3)subdPath[i]).z + sample));
			final.Add ((Vector3)subdPath[i]);
		}
		
		lineRenderer.SetVertexCount (final.Count);
		for (int i = 0; i < final.Count; i++) {
			lineRenderer.SetPosition(i, (Vector3)final[i]);			
		}
	}

	Vector3[] ComputePath(Vector3 startingPosition, Vector3 startingDirection, int depth = 0){
		if (depth > maxDepth)
			return new Vector3[0];

		// Compute closest hit, if exists
		Vector3[] res;
		RaycastHit[] hits = Physics.RaycastAll(startingPosition, startingDirection, maxLength);
		KeyValuePair<int, int> hitIndex = findFirstHit (hits);

		if (hitIndex.Key == -1) {
			res = new Vector3[2];
			res[0] = startingPosition;
			res[1] = startingPosition + maxLength * startingDirection;
			return res;
		}
		if (hitIndex.Key == -2) {
			res = new Vector3[2];
			res[0] = startingPosition;
			res[1] = hits[hitIndex.Value].point;
			return res;
		}
		else {
			Vector3[] temp = ComputePath(hits[hitIndex.Value].point,  Vector3.Reflect(startingDirection, hits[hitIndex.Value].normal), depth + 1);
			res = new Vector3[temp.Length + 1];
			res[0] = startingPosition;
			int j = 1;
			foreach (Vector3 v in temp){ res[j] = temp[j-1]; j++;}
			return res;
		}
	}

	KeyValuePair<int, int> findFirstHit(RaycastHit[] hits){
		int i = 0;
		float minDistance = Mathf.Infinity;
		KeyValuePair<int, int> minHit = new KeyValuePair<int, int>();
		while(i < hits.Length){
			if(!hits[i].collider.isTrigger) {
				if (hits[i].distance < minDistance){
					minDistance = hits[i].distance;
					minHit = new KeyValuePair<int, int>(0, i);
				}
			}
			i++;
		}

		if ((minHit.Key != -1) && (hits[minHit.Value].transform.gameObject.tag == "Receiver")) {
			hits[minHit.Value].transform.SendMessage("laserCollision", new Vector3(color.r, color.g, color.b));
			return new KeyValuePair<int, int>(-2, minHit.Value);
		}

		return new KeyValuePair<int, int>(0, minHit.Value);
	}
}