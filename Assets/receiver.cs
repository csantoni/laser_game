﻿using UnityEngine;
using System.Collections;

public class receiver : MonoBehaviour {

	public Material material;

	Color defaultColor;
	bool isHit;
	// Use this for initialization
	void Start () {
		defaultColor = material.GetColor ("_Color");
		isHit = false;
	}
	
	// Update is called once per frame
	void Update () {
		material.SetColor ("_Color", defaultColor);
	}

	void laserCollision(Vector3 color){
		Debug.Log("Hey! A laser hit me, with color (" + color.x + ", "  + color.y + ", "  + color.z + ")");
		material.SetColor ("_Color", new Color (color.x, color.y, color.z));
		isHit = true;
	}
}
